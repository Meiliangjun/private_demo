//
//  JMSayHello.m
//  PrivatePodsExample
//
//  Created by Liangjun Mei on 2019/4/16.
//  Copyright © 2019 Liangjun Mei. All rights reserved.
//

#import "JMSayHello.h"

@implementation JMSayHello

+ (void)toSay:(NSString *)words {
    NSLog(@"Hello, %@", words);
    
    // s
}

+ (void)sayHelloWorld {
    
}

@end
