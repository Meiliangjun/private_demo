//
//  MyGroup0.m
//  PrivatePodsExample
//
//  Created by Liangjun Mei on 2019/4/19.
//  Copyright © 2019 Liangjun Mei. All rights reserved.
//

#import "MyGroup0.h"

@implementation MyGroup0

+ (void)myGroupMethod {
    NSLog(@"This is my group method.");
}

@end
