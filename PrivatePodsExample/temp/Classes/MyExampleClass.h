//
//  MyExampleClass.h
//  PrivatePodsExample
//
//  Created by Liangjun Mei on 2019/4/19.
//  Copyright © 2019 Liangjun Mei. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyExampleClass : NSObject

+ (void)myMethod;

@end

NS_ASSUME_NONNULL_END
