//
//  MyExampleClass.m
//  PrivatePodsExample
//
//  Created by Liangjun Mei on 2019/4/19.
//  Copyright © 2019 Liangjun Mei. All rights reserved.
//

#import "MyExampleClass.h"

@implementation MyExampleClass

+ (void)myMethod {
    NSLog(@"This my method...");
}

@end
