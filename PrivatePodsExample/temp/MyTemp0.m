//
//  MyTemp0.m
//  PrivatePodsExample
//
//  Created by Liangjun Mei on 2019/4/19.
//  Copyright © 2019 Liangjun Mei. All rights reserved.
//

#import "MyTemp0.h"

@implementation MyTemp0

+ (void)myTempMethod {
    NSLog(@"This is my temp method.");
}

@end
