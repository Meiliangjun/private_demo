//
//  MyTemp0.h
//  PrivatePodsExample
//
//  Created by Liangjun Mei on 2019/4/19.
//  Copyright © 2019 Liangjun Mei. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyTemp0 : NSObject

+ (void)myTempMethod;

@end

NS_ASSUME_NONNULL_END
