//
//  Example.m
//  PrivatePodsExample
//
//  Created by Liangjun Mei on 2019/4/16.
//  Copyright © 2019 Liangjun Mei. All rights reserved.
//

#import "Example.h"

@implementation Example

+ (NSString *)helloWorld {
    return @"Hello, world";
}

@end
