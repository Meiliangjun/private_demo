//
//  JMSayHello.h
//  PrivatePodsExample
//
//  Created by Liangjun Mei on 2019/4/16.
//  Copyright © 2019 Liangjun Mei. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface JMSayHello : NSObject

+ (void)toSay:(NSString *)words;
+ (void)sayHelloWorld;

@end

NS_ASSUME_NONNULL_END
