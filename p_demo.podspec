Pod::Spec.new do |s|
  s.name             = "p_demo"
  s.version          = "0.0.2"
  s.summary          = "The open source fonts for Artsy apps + UIFont categories."
  s.homepage         = "https://gitlab.com/Meiliangjun/private_demo"
  s.license          = 'Code is MIT, then custom font licenses.'
  s.author           = { "Meiliangjun" => "Meiliangjun@Meiliangjun" }
  s.source           = { :git => "https://gitlab.com/Meiliangjun/private_demo.git", :tag => s.version }
  # s.social_media_url = 'https://twitter.com/artsy'

  s.platform     = :ios, '12.0'
  s.requires_arc = true

  s.source_files = 'PrivatePodsExample', 'PrivatePodsExample/temp/*.{h,m}'
  # s.resources = 'Pod/Assets/*'

  s.frameworks = 'UIKit', 'CoreText'
  s.module_name = 'private_demo'
end